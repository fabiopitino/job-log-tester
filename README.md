# Job log tester

Simple script to test GitLab's job logs

```
ruby job_log_tester.rb [<iterations> [<testcase>]]

Examples:

  # Run all test cases with defaul 10 iterations
  ruby job_log_tester.rb 

  # Run specific tests with with 20 iterations for each test
  ruby job_jog_tester.rb 20 test_xterm_fg_colors test_white_spaces
```
