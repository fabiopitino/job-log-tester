# frozen_string_literal: true

# This script generates and example of job log with Ansi escape codes
# and GitLab Runner log sections.
#
# Usage via command line:
# ruby job_log_tester.rb [<iterations> [<testcase>...]]
class JobLogTester
  COLORS = {
    0 => 'black',
    1 => 'red',
    2 => 'green',
    3 => 'yellow',
    4 => 'blue',
    5 => 'magenta',
    6 => 'cyan',
    7 => 'white'
  }.freeze

  STYLE_SWITCHES = {
    bold: 1,
    italic: 3,
    underline: 4,
    conceal: 8,
    cross: 9
  }.freeze

  SHORT_LINE = 'Lorem ipsum dolor sit amet'
  PARAGRAPH = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lorem dolor, congue ac condimentum vitae, semper gravida ipsum. Integer feugiat tellus lacus, non ullamcorper nibh volutpat id. Curabitur nec dolor lacinia, pretium massa a, sagittis metus. Sed sed sapien eget nisl rhoncus placerat. Vestibulum sodales, metus nec efficitur pharetra, neque lorem consectetur ligula, in pretium nisi diam ut arcu. Praesent convallis ac nisi a commodo.'

  attr_reader :iterations, :tests

  def initialize(iterations: 10, tests: [])
    @iterations = iterations
    @tests = Array(tests)
  end

  def run
    @tests = all_test_cases if tests.empty?

    @tests.each do |test_case|
      puts "== #{test_case.to_s.upcase} =="
      send(test_case)
      puts ''
    end
  end

  def all_test_cases
    methods.select { |m| m.to_s.start_with?('test_') }
  end

  def test_short_lines
    iterations.times { puts SHORT_LINE }
  end

  def test_long_lines
    iterations.times { puts PARAGRAPH }
  end

  def test_many_lines
    (iterations * 10).times do |n|
      puts "#{n}: #{SHORT_LINE}"
    end
  end

  def test_fg_colors
    iterations.times do |n|
      c = COLORS.keys[n % COLORS.size]
      puts "#{ansi("3#{c}")}#{SHORT_LINE}#{ansi(0)}"
    end
  end

  def test_multiple_lines_with_same_color
    iterations.times do |n|
      c = COLORS.keys[n % COLORS.size]
      puts "#{ansi("3#{c}")}#{Array.new(10) { SHORT_LINE }.join("\n")}#{ansi(0)}"
    end
  end

  def test_bg_colors
    iterations.times do |n|
      c = COLORS.keys[n % COLORS.size]
      puts "#{ansi("4#{c}")}#{SHORT_LINE}#{ansi(0)}"
    end
  end

  def test_switching_styles
    iterations.times do |n|
      s = STYLE_SWITCHES.values[n % STYLE_SWITCHES.size]
      puts "#{ansi(s)}#{SHORT_LINE}#{ansi(0)}"
    end
  end

  def test_white_spaces
    iterations.times do |n|
      puts ' ' * n + 'x'
    end
  end

  def test_adding_and_removing_styles
    puts ansi(0) + SHORT_LINE
    STYLE_SWITCHES.values.each do |v|
      puts ansi(v) + SHORT_LINE
    end

    STYLE_SWITCHES.values.reverse_each do |v|
      puts ansi("2#{v}") + SHORT_LINE
    end
    puts ansi(0) + SHORT_LINE
  end

  def test_random_ansi
    iterations.times do
      PARAGRAPH.chars.each do |c|
        print ansi(0) + random_colors + c + ansi(0)
      end
    end
  end

  def test_xterm_fg_colors
    (iterations / 10 * 255).times do |n|
      print ansi("38;5;#{n}") + 'x'
    end
  end

  def ansi(sequence)
    "\e[#{sequence}m"
  end

  def random_colors
    sequence = []
    fg = [nil, *COLORS.keys].sample
    bg = [nil, *COLORS.keys].sample
    sequence << "3#{fg}" if fg
    sequence << "4#{bg}" if bg

    ansi sequence.compact.join(';')
  end

  def self.section(description)
    @@sections << -> {
      puts "== #{description.upcase} =="
      yield
      puts ''
    }
  end
end

iterations, *tests = ARGV
iterations ||= 10

JobLogTester.new(iterations: iterations.to_i, tests: tests).run
